# Custom dmenu build

`dmenu` built with some of my preferred defaults added to the `config.def.h`:  
  - Nord colorscheme  
  - Meslo font
  - Hard coded height of 24px

## Installing

```
git clone https://github.com/seesleestak/dmenu.git && cd st
make && sudo make install
```
